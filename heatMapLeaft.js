window.onload = function() {
    var testData = {
        max: 100,
        min:0,
        data: [{lat: -16.889556, lng:-55.679554, count: 1} ,{lat: -23.56593, lng:-46.68745, count: 2},{lat: -22.8102, lng:-43.4143, count: 3},{lat: -27.0179, lng:-49.0448, count: 1} ,{lat: -7.0256, lng:-37.2765, count: 1} ,{lat: -30.0652, lng:-51.2080, count: 1},{lat: -6.76996, lng:-48.29462, count: 1}]
    };

    var baseLayer = L.tileLayer(
        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
            maxZoom: 18
        }
    );

    var cfg = {
        // radius should be small ONLY if scaleRadius is true (or small radius is intended)
        "radius": 2,
        "maxOpacity": .8,
        // scales the radius based on map zoom
        "scaleRadius": true,
        // if set to false the heatmap uses the global maximum for colorization
        // if activated: uses the data maximum within the current map boundaries
        //   (there will always be a red spot with useLocalExtremas true)
        "useLocalExtrema": true,
        // which field name in your data represents the latitude - default "lat"
        latField: 'lat',
        // which field name in your data represents the longitude - default "lng"
        lngField: 'lng',
        // which field name in your data represents the data value - default "value"
        valueField: 'count'
    };


    var heatmapLayer = new HeatmapOverlay(cfg);

    var map = new L.Map('map-canvas', {
        center: new L.LatLng(-18.45, -50.30),
        zoom: 4,
        layers: [baseLayer, heatmapLayer]
    });

    heatmapLayer.setData(testData);
};
